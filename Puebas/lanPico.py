from machine import Pin
import time

s1Pin = Pin(0, Pin.OUT, Pin.PULL_DOWN)   
s2Pin = Pin(1, Pin.OUT, Pin.PULL_DOWN)                            #Pin Swicht diodo Mosfet 1
s3Pin = Pin(2, Pin.OUT, Pin.PULL_DOWN)                            #Pin Swicht diodo Mosfet 2

def zfl(s, width):                                     #Funcion para completar con 0 a la derecha una cadena
    return '{:0>{w}}'.format(s, w=width)

def conversion(numero):
    binario = ""
    while numero // 2 != 0:                            # mientras el numero de entrada sea diferente de cero
        binario = str(numero % 2) + binario            # paso 1: dividimos entre 2
        numero = numero // 2
    return str(numero) + binario

def run():
    numero = int(input ('numero: '))
    dato= conversion(numero)
    ins= zfl(dato, 3)
    while(1):
        for i in range(len(ins)):
            bit = ins[i]
            if bit == "1":
                if i == 2:
                    s1Pin.value(1)
                if i == 1:
                    s2Pin.value(1)
                if i == 0:
                    s3Pin.value(1)
            else:
                if i == 2:
                    s1Pin.value(0)
                if i == 1:
                    s2Pin.value(0)
                if i == 0:
                    s3Pin.value(0)
        time.sleep(0.5)
        break


if __name__ == '__main__':
    run()
