menu= """
    Escribe el numero del mosfet a conmutar
        1. mosfet1
        2. mosfet2
"""


from machine import Pin
from machine import ADC
import time
from time import sleep_ms


s1 = Pin(2, Pin.OUT, Pin.PULL_DOWN)                              #Pin de control multiplexor para ADC
s2 = Pin(3, Pin.OUT, Pin.PULL_DOWN)                              #Pin de control multiplexor para ADC

def run():
    
    while True:
        print(menu)
        numero = int(input ('Opcion: '))
        if (numero == 1):
            s1.value(1)
            time.sleep(0.9)
            s2.value(0)
        if (numero == 2):
            s2.value(1)
            time.sleep(0.9)
            s1.value(0)
        

if __name__ == '__main__':
    run()