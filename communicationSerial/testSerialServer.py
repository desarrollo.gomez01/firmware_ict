menu= """
    IN CIRCUIT TEST
    OTTIS HARDWARE 2021
"""
from ctypes import Array
import serial
ser = serial.Serial()                       #escoge ser como variable serial
ser.baudrate=9600                           #se escoge el baudrate con el que se recibira y enviara datos
ser.port = 'COM5'                           #se escoge el puerto serial por el que se enviaran los datos
ser.open()                                  #se abre el puerto
while True:
    print(menu)
    D=input("Cuantas veces quiere realizar test de DIODOS : " ) 
    B=input("Cuantas veces quiere realizar test de BAJO VOLTAJE : " )
    I=input("Cuantas veces quiere realizar test de Inversion de voltaje : " )
    P=input("Cuantas veces quiere realizar test de fusibles reseteables: " )
    cnt=0                                   #contador que nos permitira recibir las 4 trama de datos de la pico
    arr=[D,B,I,P]                           #se guarda la informacion de los testeos en un vector
    datos =' '.join(arr)                    #se unen los datos de el vector en un string ,cada valor esta separado por un espacio
    tramadedatos = bytes(datos, 'utf-8')    # se codifican el dato string en bytes
    ser.write(tramadedatos)                 #se envian los datos codificados al ser.port
    while cnt<4:
        x=ser.read(9)                       #se espera a recibir los resultados desde la pico,9 bytes
        tramadedatos=x.decode("utf-8")      #se decodifican los bytes recibidos a tipo string
        testeos=tramadedatos.split(" ")     #se separan en un vector donde cada posicion se determina por un espacio
        #la trama de datos recibidas contara con un identidicador que nos dira a que testeo corresponden los datos
        if testeos[0]=='D': 
            print('El testeo de diodos se realizo correctamente: '+ testeos[1] +' Incorrectamente: '+ testeos[2] +' no se pudo realizar: ' +testeos[3])
        elif testeos[0]=='B':
            print('El testeo de Bajo voltaje se realizo correctamente: '+ testeos[1] +' Incorrectamente: '+ testeos[2] +'no se pudo realizar: ' +testeos[3])
        elif testeos[0]=='I':
            print('El testeo de inversion se realizo correctamente: '+ testeos[1] +' Incorrectamente: '+ testeos[2] +' No se pudo realizar: ' +testeos[3])
        elif testeos[0]=='P':
            print('El testeo de PTCs se realizo correctamente: '+ testeos[1] +'Incorrectamente: '+ testeos[2] +'No se pudo realizar: ' +testeos[3])
        cnt+=1

    