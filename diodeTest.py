menu= """
    Escribe el numero del diodo que quieres testear 
        1. Diodo 1
        2. Diodo 2
        3. salir
"""

from machine import Pin
from generalFunctions import analog_digital, r_desplazamiento, initial_data, multiplexorADC
#from Puebas.pruebas import multiplexorADC
import time
from generalFunctions import timeProteccions

#s_D1 = Registro de desplazamiento Q0        #Pin Swicht diodo Mosfet 1
#s_D2 = Registro de desplazamiento Q1        #Pin Swicht diodo Mosfet 2
#s_D3 = Registro de desplazamiento Q3        #Pin Swicht comun tierra

def diodo1():
    r_desplazamiento(233)                   #Activa las salidas digitales correspondientes al testeo diodo 1
    time.sleep(timeProteccions)             #tiempo de espera
    multiplexorADC(5)                       #Activa la salidad correspondiente al ADC para lectura de diodo 1
    time.sleep(timeProteccions)             #tiempo de espera
    v_diode1 = analog_digital()             #Realiza la lectura del ADC y retorna el valor
    initial_data()                          #Retorna todas las salidas digitales a los datos iniciales (todo apagado)
    time.sleep(1)                           #Tiempo de espera        
    return v_diode1

def diodo2():
    r_desplazamiento(234)                   #Activa las salidas digitales correspondientes al testeo diodo 2
    time.sleep(timeProteccions)             #tiempo de espera
    multiplexorADC(6)                       #Activa la salidad correspondiente al ADC para lectura de diodo 2
    time.sleep(timeProteccions)             #tiempo de espera
    v_diode2 = analog_digital()             #Realiza la lectura del ADC y retorna el valor
    initial_data()                          #Retorna todas las salidas digitales a los datos iniciales (todo apagado)
    time.sleep(1)                           #Tiempo de espera
    return v_diode2                      

def diodeTest():
    while True:
        print(menu)
        numero = int(input ('Opcion: '))        #enciende el mosfet de la tierra común
        if numero == 1:
            voltage1 = diodo1()
            if voltage1>0.4 and voltage1<0.8:
                print("Diodo 1 correcto")
                print (voltage1)
            else:
                print("Diodo 1 falla")
                print (voltage2)
        elif numero == 2 :
            voltage2 = diodo2()
            if voltage2>0.4 and voltage2<0.8:
                print("Diodo 2 correcto")
                print (voltage2)
            else:
                print("Diodo 2 falla")
                print (voltage2)
        else:
            initial_data()                      #Apaga el mosfet de la tierra comun
            time.sleep(0.5)
            break
        #GPIO.output(pin2, GPIO.LOW)            #Apaga el mosfet de la tierra comun


if __name__ == '__main__':
    diodeTest()
    