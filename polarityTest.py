from machine import Pin
from generalFunctions import initial_data, analog_digital, r_desplazamiento, multiplexorADC
from generalFunctions import timeProteccions
import time

#s_I1 = Registro de desplazamiento Q7           #Pin para activar el bajo voltaje
sF_positivo = Pin(6, Pin.OUT)                      #Pin de polaridad correcta
sF_negativo = Pin(7, Pin.OUT)                      #Pin de polaridad negativa

def polarity_Test():                            #Funcion para activar la proteccion de polaridad
    r_desplazamiento(108)
    time.sleep(timeProteccions)
    voltaje = analog_digital()
    time.sleep(1)
    return voltaje

def values_voltage(valor):                      #Funcion de comparación con los voltajes
    if valor < 2:
        return True
    else:
        return False

def voltage_p1():                               #Funcion para leer el voltaje P1, retorna un 1 si es correcto y un 0 si no 
    time.sleep(timeProteccions)
    multiplexorADC(3)
    valor = polarity_Test()
    v = values_voltage(valor)
    time.sleep(0.001)
    if v == True:
        v_p1 = 0
    else:
        v_p1 = 1
    return v_p1

def voltage_p2():                              #Funcion para leer el voltaje P1, retorna un 1 si es correcto y un 0 si no
    time.sleep(timeProteccions)
    multiplexorADC(4)
    valor = polarity_Test()
    v = values_voltage(valor)
    time.sleep(timeProteccions)
    if v == True:
        v_p2 = 0
    else:
        v_p2 = 1
    return v_p2

def voltage_out():                          #Funcion para leer el voltaje Vout, retorna un 1 si es correcto y un 0 si no
    time.sleep(timeProteccions)
    multiplexorADC(2)
    valor = polarity_Test()
    v = values_voltage(valor)
    time.sleep(timeProteccions)
    if v == True:
        v_out = 0
    else:
        v_out = 1
    return v_out

def positive_polarity():                        
    voltages = []
    initial_data()
    sF_positivo.value(1)                        #Test de Polaridad correcta
    voltages.append(voltage_p1())
    voltages.append(voltage_p2())
    voltages.append(voltage_out())
    if [1, 0, 1] == voltages:
        print("El circuito de Protección Funciona")
    elif[1, 0, 0] == voltages :
        print("El circuito de Protección No Funciona")
    sF_positivo.value(0)

def negative_polarity():
    voltages = []
    initial_data()
    sF_negativo.value(1)                        #Test de Polaridad correcta
    voltages.append(voltage_p1())
    voltages.append(voltage_p2())
    voltages.append(voltage_out())
    if [0, 1, 1] == voltages:
        print("El circuito de Protección Funciona")
    elif[0, 1, 0] == voltages :
        print("El circuito de Protección No Funciona")
    sF_negativo.value(0)

def polarityTest():
    initial_data()
    positive_polarity()
    negative_polarity()


if __name__ == '__main__':
    polarity_Test()