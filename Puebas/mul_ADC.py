menu=""" 
        Escribe en numero decimal el dato que quieres cargar
            a. Empezar
            b. salir
    """

from machine import Pin
from machine import ADC
import time
from time import sleep_ms


s1mul = Pin(16, Pin.OUT, Pin.PULL_DOWN)                              #Pin de control multiplexor para ADC
s2mul = Pin(17, Pin.OUT, Pin.PULL_DOWN)                             #Pin de control multiplexor para ADC
s3mul = Pin(18, Pin.OUT, Pin.PULL_DOWN)                             #Pin de control multiplexor para ADC


def analog_digital():                                 
    pot = ADC(27)                    
    cont = 1
    prom = 0
    while cont < 17:
        lectura = pot.read_u16()
        sleep_ms(100)
        cont = cont + 1
        prom = prom + lectura
        
    Promedio=prom/16
    Voltaje=Promedio*3.3/65535    
    return Voltaje


def conversion(numero):                                #Funcion de conversion decimal a binario
    binario = ""
    while numero // 2 != 0:                            #mientras el numero de entrada sea diferente de cero
        binario = str(numero % 2) + binario            #paso 1: dividimos entre 2
        numero = numero // 2     
    return str(numero) + binario           

def zfl(s, width):                                     #Funcion para completar con 0 a la derecha una cadena
    return '{:0>{w}}'.format(s, w=width)

def multiplexorADC(numero):                            #Funcion para aumentar las salidas ADC de la raspberry pi pico
    dato= conversion(numero)
    ins= zfl(dato, 3)
    print(ins)
    while True:
        for i in range(len(ins)):
            bit = ins[i]
            print(i)
            if bit == "1":
                if i == 2:
                    s1mul.value(1)
                if i == 1:
                    s2mul.value(1) 
                if i == 0:
                    s3mul.value(1)
            else:
                if i == 2:
                    s1mul.value(0)
                if i == 1:
                    s2mul.value(0) 
                if i == 0:
                    s3mul.value(0)
        time.sleep(0.05)
        break

def run():
    numero = (input("Escribe el numero de la salida del mul: " ))
    multiplexorADC(numero)

if __name__ == '__main__':
    run()