menu= """
    Escribe el numero del diodo que quieres testear 
        1. s_F12
        2. S_F24
        3. salir
"""

from machine import Pin
from generalFunctions import analog_digital, initial_data, multiplexorADC, r_desplazamiento
from generalFunctions import timeProteccions, timeWait
import time


#s_B1 = Registro de desplazamiento Q3                         #Pin para activar el bajo voltaje
#s_B2 = Registro de desplazamiento Q4                         #Pin para unir cituito de ICT a PP_8G
#s_B3 = Registro de desplazamiento Q5                         #Pin para activar testeo a 12v
#s_B4 = Registro de desplazamiento Q6                         #Pin para activar testeo a 24v
s_F12= Pin(2, Pin.OUT, Pin.PULL_DOWN)
s_F24= Pin(3, Pin.OUT, Pin.PULL_DOWN)

def testV12():                              #Funcion para encender los 12v del circuito de conmutación
    i = 0
    while i < 2:
        if s_F24.value() == 0:
            s_F12.value(1)
            return True
        else:
            s_F24.value(0)
        i= i+1

def testV24():                              #Funcion para activar los 24v del circuito de conmutación
    i = 0
    while i < 2:
        if s_F12.value() == 1:
            s_F24.value(1)
            return True
        else:
            s_F12.value(0)
        i= i+1

def general12():                                #Funcion general para lectura de 12v
    #r_desplazamiento(220)                  
    time.sleep(timeProteccions)                 #tiempo de espera
    multiplexorADC(5)                           #Activa la salidad correspondiente al ADC para lectura de 12  voltios
    time.sleep(timeProteccions)                 #tiempo de espera
    v_low_voltage12 = analog_digital()          #realiza la lectura del adc y guarda el valor de voltaje en v_low_voltage12
    initial_data()                              #retorna todas las salidas digitales a los datos iniciales (todo apagado)
    time.sleep(timeWait)                        #tiempo de espera
    return v_low_voltage12                      #retorna el valor de v_low_voltage12

def general24():                                #Funcion general para lectura de 24v
    #r_desplazamiento(234)
    time.sleep(timeProteccions)                 #tiempo de espera
    multiplexorADC(5)                           #Activa la salidad correspondiente al ADC para lectura de diodo 1
    time.sleep(timeProteccions)                 #tiempo de espera
    v_low_voltage24 = analog_digital()          #realiza la lectura del adc y guarda el valor de voltaje en v_low_voltage24
    initial_data()                              #retorna todas las salidas digitales a los datos iniciales (todo apagado)
    time.sleep(timeWait)                         #Tiempo de espera
    return v_low_voltage24                      #retorna el valor de v_low_voltage24

def testing12v():                               #Funcion general para la lectura de voltaje igual a 12
    print (menu)                                #imprime menú
    numero = int(input ('Opcion: '))            #entrada de selección del menú
    while numero == 1:                          #ciclo para testear a 12 voltios
        v_voltage12 = []                        #variable que guarda los dos valores de 12 voltios a leer
        t = testV12()                           #Enciende el circuito de conmutación a 12v
        if t == True:                           #Entra al ciclo solo cuando la función de 12v retorne True
            r_desplazamiento(220)               #Enciende las señales de control s_B2 y s_B3
            v_voltage12.append(general12())     #Activa la funcion general de 12 voltios y guarda el valor en la posición 0 del arreglo v_voltage12
            time.sleep(timeProteccions)         #Tiempo de espera 
            r_desplazamiento(212)               #Enciende las señales de control s_B1, s_B2 y s_B3
            v_voltage12.append(general12())     #Activa la funcion general de 12 voltios y guarda el valor en la posición 1 del arreglo v_voltage12
        numero = int(input ('Opcion: '))
    return v_voltage12

def testing24v():                               #Funcion general para la lectura de voltaje igual a 24
    print (menu)                                #imprime menú
    numero = int(input ('Opcion: '))            #entrada de selección del menú
    while numero == 2:                          #ciclo para testear a 12 voltios
        v_voltage24 = []                        #variable que guarda los dos valores de 24 voltios a leer
        t = testV24()                           #Enciende el circuito de conmutación a 24v
        if t == True:                           #Entra al ciclo solo cuando la función de 24v retorne True
            r_desplazamiento(234)               #Enciende las señales de control s_B2 y s_B4
            v_voltage24.append(general24())     #Activa la funcion general de 24 voltios y guarda el valor en la posición 0 del arreglo v_voltage24
            time.sleep(timeProteccions)                  #Tiempo de espera 
            r_desplazamiento(248)               #Enciende las señales de control s_B1, s_B2 y s_B4
            v_voltage24.append(general24())     #Activa la funcion general de 24 voltios y guarda el valor en la posición 1 del arreglo v_voltage24
        numero = int(input ('Opcion: '))
    return v_voltage24

def lowVoltageTest():
    voltage12 = []                              #Posicion 0 = 12v -- Posición 1 = 11v
    voltage24 = []                              #Posicion 0 = 12v -- Posición 1 = 11v
    voltage12 = testing12v()
    voltage24 = testing24v()
    if voltage12[0] == 12 and voltage12[2] <= 11:
        print("El Testeo a 12 voltios es correcto")
        print (voltage12)
    if voltage24[0] == 24 and voltage24[1] <= 22:
        print ("El Testeo a 24 voltios es correcto")
        print (voltage24)
    else:
        print("Los testeos no son correctos")

if __name__ == '_main_':
    lowVoltageTest()