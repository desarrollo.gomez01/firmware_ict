from machine import Pin
from machine import ADC
import time
from time import sleep_ms


s1mul = Pin(16, Pin.OUT, Pin.PULL_DOWN)                              #Pin de control multiplexor para ADC
s2mul = Pin(17, Pin.OUT, Pin.PULL_DOWN)                              #Pin de control multiplexor para ADC
s3mul = Pin(18, Pin.OUT, Pin.PULL_DOWN)                              #Pin de control multiplexor para ADC
s4mul = Pin(19, Pin.OUT, Pin.PULL_DOWN)                              #Pin de control multiplexor para ADC

dataPin = Pin(8, Pin.OUT, Pin.PULL_DOWN)                             #Pin de datos registro de desplazamiento
latchPin = Pin(9 , Pin.OUT, Pin.PULL_DOWN)                           #Pin de carga registro de desplazamiento
clockPin = Pin(10, Pin.OUT, Pin.PULL_DOWN)                           #Pin de clock registro de desplazamiento

timeProteccions = 0.00009
timeWait = 0.00001

def analog_digital():                                   #Funcion de conversion analogo digital para la lectura de los voltajes analogicos del ICT
    pot = ADC(27)                    
    cont = 1
    prom = 0
    while cont < 17:
        lectura = pot.read_u16()
        sleep_ms(100)
        cont = cont + 1
        prom = prom + lectura
        
    Promedio=prom/16
    Voltaje=Promedio*3.3/65535    
    return Voltaje


def conversion(numero):                                #Funcion de conversion decimal a binario
    binario = ""
    while numero // 2 != 0:                            #mientras el numero de entrada sea diferente de cero
        binario = str(numero % 2) + binario            #paso 1: dividimos entre 2
        numero = numero // 2     
    return str(numero) + binario           

def zfl(s, width):                                     #Funcion para completar con 0 a la derecha una cadena
    return '{:0>{w}}'.format(s, w=width)

def multiplexorADC(numero):                            #Funcion para aumentar las salidas ADC de la raspberry pi pico
    dato= conversion(numero)
    ins= zfl(dato, 3)
    print(ins)
    while True:
        for i in range(len(ins)):
            bit = ins[i]
            print(i)
            if bit == "1":
                if i == 3:
                    s1mul.value(1)
                if i == 2:
                    s2mul.value(1) 
                if i == 1:
                    s3mul.value(1)
                if i == 0:
                    s4mul.value(1)
            else:
                if i == 3:
                    s1mul.value(0)
                if i == 2:
                    s2mul.value(0) 
                if i == 1:
                    s3mul.value(0)
                if i == 0:
                    s4mul.value(0)
        time.sleep(0.05)
        break

def readingBAT():                                   #Funcion para testear el voltaje de la Bateria de 12v O 24v
    multiplexorADC(8)
    vReadingBAT = analog_digital
    return vReadingBAT

def reading10V():                                   #Funcion para testear el voltaje de 10v
    multiplexorADC(10)
    vReading10 = analog_digital
    return vReading10

def reading5V():                                    #Funcion para testear el voltaje de 5v
    multiplexorADC(11)
    vReading5 = analog_digital
    return vReading5

def latch():                                        #Funcion de carga para el registro de desplazamiento
    latchPin.value(1)
    time.sleep(timeWait)
    latchPin.value(0)	
    time.sleep(timeProteccions)

def clock():                                        #Funcion de clock para el registro de desplazamiento
    clockPin.value(1)
    time.sleep(timeWait)
    clockPin.value(0)
    time.sleep(timeProteccions)

def initial_data():                                 #Funcion para desactivar  señales de control de los testeos
    borra = conversion(236)
    num = zfl(borra, 8)
    print(num)
    for i in num:
        if i == "1":
            dataPin.value(1)
            clock()
        else:
            dataPin.value(0)
            clock()
    latch()
    print("carga completa")
    
def r_desplazamiento(num_r):                        #Funcion para aumentar los GPIO de la raspberry pi pico
    dato = 0
    dato= conversion(num_r)
    ins= zfl(dato, 8)
    print(ins)
    for i in ins:
        if i == "1":
            dataPin.value(1)
            clock()
        else:
            dataPin.value(0)
            clock()
    latch()
    print("carga completa")
