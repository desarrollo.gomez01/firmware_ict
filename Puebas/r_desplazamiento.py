menu=""" 
        Escribe en numero decimal el dato que quieres cargar
            a. Empezar
            b. salir
    """
from machine import Pin
from machine import ADC
import time

dataPin = Pin(8, Pin.OUT)
latchPin = Pin(9 , Pin.OUT)
clockPin = Pin(10, Pin.OUT)

def latch():
    latchPin.value(1)
    time.sleep(0.00001)
    latchPin.value(0)	
    time.sleep(0.00009)

def clock():
    clockPin.value(1)
    time.sleep(0.00001)
    clockPin.value(0)
    time.sleep(0.00009)

def conversion(numero):            #Funcion de conversion decimal a binario
    binario = ""
    while numero // 2 != 0:                            # mientras el numero de entrada sea diferente de cero
        binario = str(numero % 2) + binario            # paso 1: dividimos entre 2
        numero = numero // 2     
    return str(numero) + binario

def zfl(s, width):
    return '{:0>{w}}'.format(s, w=width)

def r_desplazamiento():
    print(menu)
    letra = (input("Escribe la opcion: " ))
    while letra == "a":
        numero= 0
        numero = int(input ('numero: '))
        dato= conversion(numero)
        ins= zfl(dato, 8)
        print(ins)
        for i in ins:
            if i == "1":
                dataPin.value(1)
                clock()
            else:
                dataPin.value(0)
                clock()
        latch()
        print("carga completa")
        letra = (input("Escribe la opcion: " )) 


def run():
    r_desplazamiento()

if __name__ == '__main__':
    run()