from machine import Pin
import time

def pin(num1, num2, num3):
    bin= [num1, num2, num3]
    return bin

p1 = Pin(pin[0], Pin.OUT, Pin.PULL_DOWN)                      #Pin control puertos lan P1 (16 reles)
p2 = Pin(pin[1], Pin.OUT, Pin.PULL_DOWN)                      #Pin control puertos lan P2 (8 reles)
p3 = Pin(pin[2], Pin.OUT, Pin.PULL_DOWN)                      #Pin Swicht diodo Mosfet P3 (4 reles)

def zfl(s, width):                                             #Funcion para completar con 0 a la derecha una cadena
    return '{:0>{w}}'.format(s, w=width)

def conversion(numero):
    binario = ""
    while numero // 2 != 0:                                    #mientras el numero de entrada sea diferente de cero
        binario = str(numero % 2) + binario                    #paso 1: dividimos entre 2
        numero = numero // 2
    return str(numero) + binario

def run():
    numero = int(input ('numero: '))
    dato= conversion(numero)
    ins= zfl(dato, 3)
    while(1):
        for i in range(len(ins)):
            bit = ins[i]
            if bit == "1":
                if i == 2:
                    p1.value(1)
                if i == 1:
                    p2.value(1)
                if i == 0:
                    p3.value(1)
            else:
                if i == 2:
                    p1.value(0)
                if i == 1:
                    p2.value(0)
                if i == 0:
                    p3.value(0)
        time.sleep(60)
        break


if __name__ == '__main__':
    run()
