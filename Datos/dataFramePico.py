#lo que recibe la pico
datos=[]
datos2=['test',0,0,0] 
Corr=0
inco=0
fail=0
#         D I B P   si 0 no se hace 
binario=b'5 1 0 3 '                             #D=test diodos B= test bajo voltaje I= Polaridad P=test PTC  Esto se recibira desde el servidor
tramarx=binario.decode("utf-8")                 #decodificamos los binarios a string
datos=tramarx.split()                           #separamos los datos y los metemos en un vector

for m in range(len(datos)):
    datos[m]=int(datos[m])                      #se pasa de tipo string a tipo entero
#se empizan los testeo
cnt=0
while cnt<datos[0]:                             #testeo diodos el numero de veces indicado en binario
    x=input("testeo correcto? s/n/f")
    if x=="s":
        Corr+=1                                 #si el testeo se hizo correctamente se suma al contador de correctos
    if x=="n":                              
        inco+=1                                 #si el testeo se hizo correctamente y fallo se suma al incorrectos
    if x=='f':                                   
        fail+=1                                 #si el testeo no se hizo se suma al contador de Fallos
    datos2=['D',str(Corr),str(inco),str(fail)]  #el primer valor del vector es el identificador del test y se pasan  a string para codificarlos
    resultados=" ".join(datos2)                 # se unen los valores del vector con un espacio entre ellos para que al decodificarlos desde el servidor sea mas facil
    tx=resultados.encode()                      # se codifica en binario los datos para poder ser enviados por el puerto serial
    cnt+=1
print(tx)
cnt=0

while cnt<datos[1]:                             #testeo inversion de polaridad 
    test = "I 1 0 0"
    tx=test.encode()
    cnt+=1
cnt=0
print(tx)
while cnt<datos[2]:                             #testeo bajo voltaje
    test = "B 0 0 0"
    tx=test.encode()
    cnt+=1
cnt=0
print(tx)
while cnt<datos[3]:                             #testeo PTC
    test = "P 1 1 1"
    tx=test.encode()
    cnt+=1
print(tx)                                       #se envia datos
