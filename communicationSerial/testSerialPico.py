from machine import UART, Pin,ADC
from diodeTest import diodo1,diodo2
import time

uart0 = UART(0, baudrate=9600, tx=Pin(0), rx=Pin(1))

while True:
    rxData=bytes()                                  #se inicializa la variable que recibira los datos del servidor
    while uart0.any() > 0:                          #se ejecutara siempre y cuando haya algo en el puerto serial
        rxData += uart0.read()                      #se leera cada byte que se recibe en el puerto serial y se almacenara en rxdata
        tramadatos=rxData.decode("utf-8")           #se decodifica los datos recibidos a un tipo string
        testeos=tramadatos.split(" ")               #se separan los datos cada vez que se detecte un espacio en el string
        for m in range(len(testeos)):               #se pasan los valores que se necesiten a tipo entero
            testeos[m]=int(testeos[m]) 
        print(testeos)                              # cada posicion de la lista mostrara las veces que se realizara cada testeo :[diodos,bajovoltaje,inversion,PTC]
#-------------------------------Diodos
        cnt=0
        resultados=[0,0,0]
        tramatx=['',0,0,0,'']                       #se incializa la trama de datos que se enviara al servidor
        while cnt<testeos[0]:
            tramatx[0]='D'                          #se escoge el identificador del testeo
            tramatx[4]='C'                          #se escoge un valor inicial para el indicador de falla
            resultados=diodo1()                     #se inicia con el testeo del diodo uno y se retorna un vector con [Correctas,Incorrectas,No se hcicieron]
            tramatx[1]=tramatx[1]+resultados[0]     #se le suman al vector que se enviarar al servidor
            tramatx[2]=tramatx[2]+resultados[1]
            resultados=diodo2()
            tramatx[1]=tramatx[1]+resultados[0]     #se inicia con el testeo del diodo 2 y se retorna un vector con [Correctas,Incorrectas,No se hcicieron]
            tramatx[2]=tramatx[2]+resultados[1]     #se le suman al vector que se enviara al servidor
            cnt+=1
        for m in range(len(tramatx)):               #se tranforman los valores del vector a string
            tramatx[m]=str(tramatx[m])
        datos =' '.join(tramatx)                    # se unen los valores en un solo string separados por un espacio
        tramadedatos=bytes(datos,'utf-8')           #se transforman los datos a tipo bytes para poder ser enviados por el puerto serial
        uart0.write(tramadedatos)                   #se envia el archivo por el puerto serial del servidor
        
#--------------------------------------Bajo Voltaje
        cnt=0
        tramatx=['',0,0,0,'']
        while cnt<testeos[1]:
            tramatx[0]='B'
            tramatx[4]='C'
            T=input("se hizo correctamente el testeo? s/n:")
            if T=='s':
                tramatx[1]+=1
            elif T=='n':
                tramatx[2]+=1
            else:
                tramatx[3]+=1
            cnt+=1
        for m in range(len(tramatx)):
            tramatx[m]=str(tramatx[m])
        datos =' '.join(tramatx)
        tramadedatos=bytes(datos,'utf-8')
        uart0.write(tramadedatos)
        cnt=0
#~--------------------------------- Inversion
        tramatx=['',0,0,0,'']
        cnt=0
        while cnt<testeos[2]:
            tramatx[0]='I'
            tramatx[4]='C'
            T=input("se hizo correctamente el testeo? s/n:")
            if T=='s':
                tramatx[1]+=1
            elif T=='n':
                tramatx[2]+=1
            else:
                tramatx[3]+=1
            cnt+=1
        for m in range(len(tramatx)):
            tramatx[m]=str(tramatx[m])
        datos =' '.join(tramatx)
        tramadedatos=bytes(datos,'utf-8')
        uart0.write(tramadedatos)
#--------------------------------------------PTC
        tramatx=['',0,0,0,'']
        cnt=0
        while cnt<testeos[3]:
            tramatx[0]='P'
            tramatx[4]='C'
            T=input("se hizo correctamente el testeo? s/n:")
            if T=='s':
                tramatx[1]+=1
            elif T=='n':
                tramatx[2]+=1
            else:
                tramatx[3]+=1
            cnt+=1
        for m in range(len(tramatx)):
            tramatx[m]=str(tramatx[m])
        datos =' '.join(tramatx)
        tramadedatos=bytes(datos,'utf-8')
        uart0.write(tramadedatos)