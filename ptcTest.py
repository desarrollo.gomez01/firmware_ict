from machine import Pin,UART,ADC
from generalFunctions import analog_digital,multiplexorADC, shift_registerPtc
import time
from generalFunctions import timeProteccions

timePTC = 0.5

s_ds1=  Pin(12, Pin.OUT)      #DATOS 
load1 = Pin(13, Pin.OUT)     #Carga de datos 
clk1 = Pin(14, Pin.OUT)     #Reloj registro de desplazamiento 
s_clr1 = Pin(15, Pin.OUT)     # MASTERCLEAR
s_ptc9 = Pin(16,Pin.OUT)
s_clr1.value(1)

def PTC9(): # prende el mosfet de conexion a tierra de los ptc y lee el voltaje en las resistencias
    s_ptc9.value(0)
    cnt=0
    v_voltage=[] #  
    while cnt<4:
        multiplexorADC(1) # escoge la posicion Y1 del multiplexor adc
        v_ptc=analog_digital()
        v_voltage[cnt]=v_ptc
        cnt+=1

    if  v_voltage[0] > v_voltage[3]: 
            shift_registerPtc(0) #apagan todos los PTC
            s_ptc9.value(1)
            return True #el testo del ptc se hizo correctamente
        
    else:
        s_ptc9.value(1)
        shift_registerPtc(0)
        return False #el testeo fallo 

#activan cada ptc
def PTC1():
    shift_registerPtc(1)
    time.sleep()
    resultado=PTC9()
    return resultado

def PTC2():
    shift_registerPtc(2)
    time.sleep(timeProteccions)
    resultado=PTC9()
    return resultado

def PTC3():
    shift_registerPtc(4)
    time.sleep(timeProteccions)
    resultado=PTC9()
    return resultado

def PTC4():
    shift_registerPtc(8)
    time.sleep(timeProteccions)
    resultado=PTC9()
    return resultado

def PTC5():
    shift_registerPtc(16)
    time.sleep(timeProteccions)
    resultado=PTC9()
    return resultado

def PTC6():
    shift_registerPtc(32)
    time.sleep(timeProteccions)
    resultado=PTC9()
    return resultado

def PTC7():
    shift_registerPtc(64)
    time.sleep(timeProteccions)
    resultado=PTC9()
    return resultado

def PTC8():
    shift_registerPtc(128)
    time.sleep(timeProteccions)
    resultado=PTC9()
    return resultado

def ptcTest():
    while True: #veces que se mande desde el serial
        PTC1()
        time.sleep(timePTC)
        PTC2()
        time.sleep(timePTC)
        PTC3()
        time.sleep(timePTC)
        PTC4()
        time.sleep(timePTC)
        PTC5()
        time.sleep(timePTC)
        PTC6()
        time.sleep(timePTC)
        PTC8()
        time.sleep(timePTC)
        PTC8()
        time.sleep(timePTC)
    

if __name__ == '__main__':
    ptcTest()
    