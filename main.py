menu= """
    Escribe el numero del circuito que desea testear 
        1. diodeTest
        2. lowVoltageTest
        3. polarityTest
        4. ptcTest
        5. todos
        5. salir
"""
import time
from diodeTest import diodeTest
from lowVoltageTest import lowVoltageTest
from polarityTest import polarityTest
from ptcTest import ptcTest
from lowVoltageTest import reading12V, readingBAT, reading10V, reading5V

def run():
    v_BAT = readingBAT()                            #Almacena el valor de voltaje de la bateria
    v_10 = reading10V()                             #Almacena el valor de la fuente de 10v
    v_5 = reading5V()                               #Almacena el valor de la fuente de 5V
    
    #------Estado posibles para la maquina de Estados
    configuracion = "C"                             #Estado de configuración
    fin = "F"                                       #Estado de fin
    testear = "T"                                    #Estado de testeo
    start = "S"                                     #Estado de inicio de conexión
    estado = ""                                     #Variable usada para guardar el estado
    #---------Maquina de estado para configuración serial
    while True:
        if (estado == start):
            estado = configuracion
        if(estado == configuracion):
            estado = testear
        if(estado == testear):
            testeo()
            estado = fin
        if(estado == fin):
            estado = ""

def testeo():
    v_10 = reading10V()
    while True:
        print(menu)
        numero = int(input ('Opcion: '))
        if numero == 1:
            if v_10 > 0.9 and v_10 < 1.2:
                diodeTest()
            else:
                print("No puede realizarse el testeo de diodos por que el voltaje de 10v falla")
        if numero == 2:
            lowVoltageTest()
        if numero == 3:
            polarityTest()
        if numero == 4:
            ptcTest()
        if numero == 5:
            diodeTest()
            time.sleep(0.00009)
            polarityTest()
            time.sleep(0.00009)
            lowVoltageTest()
            time.sleep(0.00009)
            ptcTest()
        else: 
            break

if __name__ == '__main__':
    run()