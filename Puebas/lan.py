import RPi.GPIO as GPIO
import time

s1Pin = 7                            
s2Pin = 11                           
s3Pin = 13
s4Pin = 14                           
GPIO.setmode(GPIO.BOARD)
GPIO.setup(s1Pin, GPIO.OUT)
GPIO.setup(s2Pin, GPIO.OUT)
GPIO.setup(s3Pin, GPIO.OUT)
GPIO.setup(s4Pin, GPIO.OUT)

def conversion(numero):
    binario = ""
    while numero // 2 != 0:                            # mientras el numero de entrada sea diferente de cero
        binario = str(numero % 2) + binario            # paso 1: dividimos entre 2
        numero = numero // 2
    return str(numero) + binario

def run():
    numero = int(input ('numero: '))
    dato= conversion(numero)
    ins= dato.zfill(3)
    print(ins)
    while(1):
        for i in range(len(ins)):
            bit = ins[i]
            print(i)
            if bit == "1":
                if i == 3:
                    GPIO.output(s1Pin, GPIO.HIGH)
                if i == 2:
                    GPIO.output(s2Pin, GPIO.HIGH)
                if i == 1:
                    GPIO.output(s3Pin, GPIO.HIGH)
                if i == 0:
                    GPIO.output(s4Pin, GPIO.HIGH)    
            else:
                if i == 3:
                    GPIO.output(s1Pin, GPIO.LOW)
                if i == 2:
                    GPIO.output(s2Pin, GPIO.LOW)
                if i == 1:
                    GPIO.output(s3Pin, GPIO.LOW)
                if i == 0:
                    GPIO.output(s4Pin, GPIO.LOW)
        time.sleep(1)
        GPIO.cleanup()
        time.sleep(0.5)
        break


